#!/usr/bin/env python

import os
import requests

# Use environment variables that your CI server provides to the key, name,
# and url parameters, as well as commit hash. (The values below are used by
# Semaphore.)
data = {
    'key': os.getenv('SEMAPHORE_BUILD_NUMBER'),
    'name': os.getenv('SEMAPHORE_PROJECT_NAME'),
}

data['url'] = ('https://semaphoreci.com/%(repo_slug)s/branches/%(branch)s/builds/%(build)s'
       % {'repo_slug': os.getenv('SEMAPHORE_REPO_SLUG'),
          'branch': os.getenv('BRANCH_NAME'),
          'build': os.getenv('SEMAPHORE_BUILD_NUMBER')})

result = os.getenv('SEMAPHORE_THREAD_RESULT')

if result == 'failed':
    data['state'] = 'FAILED'
elif result == 'passed':
    data['state'] = 'SUCCESSFUL'
else:
    data['state'] = 'INPROGRESS'

# Construct the URL with the API endpoint where the commit status should be
# posted (provide the appropriate owner and slug for your repo).
api_url = ('https://api.bitbucket.org/2.0/repositories/'
           '%(owner)s/%(repo_slug)s/commit/%(revision)s/statuses/build'
           % {'owner': os.getenv('BITBUCKET_REPOSITORY_OWNER'),
              'repo_slug': os.getenv('BITBUCKET_REPOSITORY_NAME'),
              'revision': os.getenv('REVISION')})

# Post the status to Bitbucket. (Include valid credentials here for basic auth.
# You could also use team name and API key.)
requests.post(api_url, auth=(os.getenv('BITBUCKET_USERNAME'), os.getenv('BITBUCKET_PASSWORD')), json=data)
