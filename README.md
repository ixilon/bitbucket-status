# bitbucket-status #

Integrates your continuous integration build system with Bitbucket.

### What is this repository for? ###

* Update Bitbucket build status indicator depending on the outcome of the build.
* Supports Semaphore CI at the moment, more build systems will follow ...

### How do I get set up? ###

* Add the following environment variables to your project setting:
    * BITBUCKET_REPOSITORY_OWNER
    * BITBUCKET_REPOSITORY_NAME
    * BITBUCKET_USERNAME
    * BITBUCKET_PASSWORD
* Add to your build setup:
    * git clone https://bitbucket.org/ixilon/bitbucket-status.git
* Add as pre-build and post-build command:
    * python bitbucket-status/bitbucket-status.py
    
